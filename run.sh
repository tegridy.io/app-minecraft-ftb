#!/bin/bash

# change into minecraft dir
cd /minecraft

# download and install modpack
if [[ ! -e version.json || `cat version.json | jq -r '.id'` != "$MODPACK_VER" ]]; then
  wget --no-check-certificate https://api.modpacks.ch/public/modpack/$MODPACK_ID/$MODPACK_VER/server/linux -O ./install
  chmod +x ./install
  ./install $MODPACK_ID $MODPACK_VER --auto
  sed -i '/bash\|java/!d' ./start.sh
fi

# start minecraft
echo "eula=true" > eula.txt
./start.sh
