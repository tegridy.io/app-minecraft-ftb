IMAGE_REGISTRY="registry.gitlab.com/tegridy.io"

NAME="app-minecraft-ftb"
TAG=$$(git describe --tags --abbrev=0)

image:
	docker build -t ${IMAGE_REGISTRY}/${NAME}:${TAG} -t ${IMAGE_REGISTRY}/${NAME}:latest .

image-push:
	docker push ${IMAGE_REGISTRY}/${NAME}:${TAG}
	docker push ${IMAGE_REGISTRY}/${NAME}:latest

helm:
	helm package ./charts
