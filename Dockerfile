FROM openjdk:8-jdk-bullseye

ENV MODPACK_ID=91
ENV MODPACK_VER=2089

EXPOSE 25565

VOLUME /minecraft

RUN apt-get update; apt-get install -y jq; apt-get clean; rm -rf /var/lib/apt/lists/*

RUN useradd -m -U minecraft
RUN chown -R minecraft:minecraft /minecraft

USER minecraft
COPY ./run.sh /run.sh
CMD /run.sh
